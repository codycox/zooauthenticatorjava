/*
 * IT-145 Zoo Authenticator
 * Cody Cox
 * Southern New Hampshire University
 */

package authenticationsystem;
import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.*;

public class ZooAuthenticator {
 
    public static void main(String[] args) throws Exception
    {
         startProgram();
    }
    
    public static void startProgram() throws Exception
      {
         //Get users input and set up the variables
         String input, usrName, pssWord="", message = "\n\nAccess denied.";
 
         // Attempts before lock out
         int chances = 3;
         
         //New scanner object
         Scanner scnr = new Scanner(System.in); 
 
         while(chances > 0)
            {
               // Get Username
               System.out.print("\nEnter your username: "); 
               usrName = scnr.nextLine(); 
 
               // Get password
               System.out.print("\nEnter your password: ");
               pssWord = scnr.nextLine(); 
 
               
               // uncomment to test
               //System.out.println("\n\n"+userName+" "+passWord); 
               
               String encrypted = makeMD5(pssWord);
               
               // uncomment for testing md5 password
               //System.out.printf("Password %s becomes %s \n",passWord,encrypted);
               
               String myResult = passWordCheck(encrypted,usrName);
               
               // Uncomment to test result
               //System.out.println(myResult);

               if(myResult.equals("fail"))
                  {
                     chances--;
                     System.out.printf("\nYou have %d chances left.\n", chances);
                  }
               else
                  {
                     message = "You now have access!";
                     break;
                  }
            }
         System.out.println(message);
         if(!message.equals("You now have access!"))
            {
               System.exit(0);
            }
      }
 
    /**
    * Run after access
    * @param job accepts job description
    * @throw java.lang.Exception
    */
   public static void accessGranted(String job) throws Exception
      {
         Scanner scnr = new Scanner(System.in);
         String userinput="";
         File zfile = new File("/Users/Cody/Desktop/SNHU/ZooAuthenticator/zookeeper.txt"); 
         File vfile = new File("/Users/Cody/Desktop/SNHU/ZooAuthenticator/veterinarian.txt"); 
         File afile = new File("/Users/Cody/Desktop/SNHU/ZooAuthenticator/admin.txt"); 
         BufferedReader br;
         System.out.println("\nYou may access all features applicable to your position. Enter 'q' to quit and logout.");
         System.out.printf("\nYour position: %s\n",job);
         
         if(job.equals("admin"))
            {
               FileReader fr = new FileReader(afile); 
               int i; 
               while ((i=fr.read()) != -1) 
                  {
                     System.out.print((char) i);
                  } 
           }
         if(job.equals("veterinarian"))
            {
               FileReader fr = new FileReader(vfile); 
               int i; 
               while ((i=fr.read()) != -1) 
                  {
                     System.out.print((char) i);
                  } 
            } 
         
         else
            {
               FileReader fr = new FileReader(zfile); 
               int i; 
               while ((i=fr.read()) != -1) 
                  {
                     System.out.print((char) i);
                  } 
            } 
         while(!"q".equals(userinput) || !"Q".equals(userinput))
            {
               System.out.print("\nEnter 'q' to log out: ");
               userinput = scnr.nextLine();
               System.out.printf("You have entered %s \n", userinput);
               if(userinput.equals("q") || userinput.equals("Q"))
                  {
                     System.out.println("Goodbye!");
                     System.exit(0);
                  }
            }
      }
      
    /* Process the password */
    /**
    * @param pwInput Stores incoming password
    * @return returns value of md5 encrypted password 
    * @throws java.security.NoSuchAlgorithmException
    */
   public static String makeMD5(String pwInput) throws NoSuchAlgorithmException
      {
         // Change "password" to user input
         String original = pwInput; 
         MessageDigest md = MessageDigest.getInstance("MD5");
         md.update(original.getBytes());
         byte[] digest = md.digest();
         StringBuilder sb = new StringBuilder();
         
         for (byte b : digest) 
            {
               sb.append(String.format("%02x", b & 0xff));
            }
         return sb.toString();
      }
 
   public static String passWordCheck(String pwCheck, String unCheck) throws Exception
      {
         String results = "fail";
         String tester;
         File file = new File("/Users/Cody/Desktop/SNHU/ZooAuthenticator/credentials.txt"); 
         File zfile = new File("/Users/Cody/Desktop/SNHU/ZooAuthenticator/zookeeper.txt"); 
         File vfile = new File("/Users/Cody/Desktop/SNHU/ZooAuthenticator/veterinarian.txt"); 
         File afile = new File("/Users/Cody/Desktop/SNHU/ZooAuthenticator/admin.txt"); 
 
         BufferedReader br = new BufferedReader(new FileReader(file));
 
         while((tester = br.readLine()) != null)
            { 
               //System.out.println(line);
               String[] credentialCheck = tester.split("\\s+");
 
                // Check for a usrname and password match up
               for (String cc : credentialCheck) 
               {
                  if (cc.equals(pwCheck)) 
                     {
                        // Get username
                        tester = credentialCheck[0]; 
 
                        if(tester.equals(unCheck))
                           {
                              System.out.println("\n\nWelcome, "+tester);
                              String last = credentialCheck[3];
                              System.out.println("\n\nJob: "+last);
                              
                              if(last.equals("admin"))
                                 {
                                    accessGranted("admin");
                                 }
                              if(last.equals("zookeeper"))
                                 {
                                    accessGranted("zookeeper");
                                 }
                              if(last.equals("veterinarian"))
                                 {
                                    accessGranted("veterinarian");
                                 }
                              results = "pass";
                          }
 
                        break;
                      }
               }
            }
         
         br.close();
         return results;
      }
      
    /*
    fileScnr.useDelimiter("\"");
    
    while (fileScnr.hasNextLine()) 
      {
         //System.out.println(fileScnr.nextLine()); // test to see if file is read
         tester = fileScnr.nextLine();
         if(pwCheck.equals(tester))
            {
               results = "Passwords match";
               System.out.println("Match found: "+tester);
               break;
            }
    }
    return results;
    }*/
 }